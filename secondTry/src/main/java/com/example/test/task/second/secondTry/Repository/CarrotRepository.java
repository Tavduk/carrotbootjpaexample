package com.example.test.task.second.secondTry.Repository;

import com.example.test.task.second.secondTry.Entity.CarrotEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Max on 21.11.2018.
 */
@Repository
public interface CarrotRepository  extends JpaRepository<CarrotEntity, Long>{

    List<CarrotEntity> findAll();

    CarrotEntity findCarrotModelByCarrotId(Long carrotId);

    void deleteByCarrotId(Long carrotId);
}
