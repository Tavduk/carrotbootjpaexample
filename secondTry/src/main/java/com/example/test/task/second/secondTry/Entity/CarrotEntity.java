package com.example.test.task.second.secondTry.Entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Max on 21.11.2018.
 */
@Entity
@Data
@Builder
public class CarrotEntity {

    @Id
    @GeneratedValue
    private Long carrotId;
    private String carrotName;
    private Long carrotAmt;
}
