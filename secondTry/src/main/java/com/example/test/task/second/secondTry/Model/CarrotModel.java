package com.example.test.task.second.secondTry.Model;

import lombok.*;
/**
 * Created by Max on 21.11.2018.
 */

@Getter
@Setter
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CarrotModel {
    private Long carrotId;
    private String carrotName;
    private Long carrotAmt;
}
