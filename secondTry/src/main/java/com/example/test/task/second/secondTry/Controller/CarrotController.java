package com.example.test.task.second.secondTry.Controller;

import com.example.test.task.second.secondTry.Model.CarrotModel;
import com.example.test.task.second.secondTry.Service.CarrotService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Max on 21.11.2018.
 */
@RestController
@RequestMapping(path = "/api/carrot")
public class CarrotController {

    private final CarrotService carrotService;

    public CarrotController(CarrotService carrotService){
        this.carrotService = carrotService;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ApiOperation("get all the carrot")
    public ResponseEntity<List<CarrotModel>> getAllCarrots() {
        List<CarrotModel> carrotModels = carrotService.findAllCarrots();
        return carrotModels != null ? ResponseEntity.ok(carrotModels) : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ApiOperation("add the carrot")
    public ResponseEntity<CarrotModel> addCarrot(@RequestBody final CarrotModel carrotModel) {
        CarrotModel loadedCarrot = carrotService.addCarrot(carrotModel);
        return loadedCarrot != null ? ResponseEntity.ok(loadedCarrot) : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    @ApiOperation("find the carrot")
    public ResponseEntity<CarrotModel> findCarrotById(Long carrotId) {
        CarrotModel foundCarrot = carrotService.findCarrotByCarrotId(carrotId);
        return foundCarrot != null ? ResponseEntity.ok(foundCarrot) : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ApiOperation("delete the carrot")
    public ResponseEntity<String> deleteCarrotById(Long carrotId) {
        String result = carrotService.deleteCarrotByCarrotyId(carrotId);

        return result != null ? ResponseEntity.ok(result) : ResponseEntity.notFound().build();

    }

}
