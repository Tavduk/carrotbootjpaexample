package com.example.test.task.second.secondTry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class SecondTryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecondTryApplication.class, args);
	}
}
