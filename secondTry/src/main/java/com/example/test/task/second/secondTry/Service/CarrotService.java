package com.example.test.task.second.secondTry.Service;

import com.example.test.task.second.secondTry.Entity.CarrotEntity;
import com.example.test.task.second.secondTry.Model.CarrotModel;
import com.example.test.task.second.secondTry.Repository.CarrotRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Max on 21.11.2018.
 */
@Service
public class CarrotService {

    private final CarrotRepository carrotRepository;

    public CarrotService(CarrotRepository carrotRepository){
        this.carrotRepository = carrotRepository;
    }

    @Transactional
    public List<CarrotModel> findAllCarrots() {
        return carrotRepository.findAll().stream()
                .map(entity -> CarrotModel.builder().carrotId(entity.getCarrotId())
                .carrotName(entity.getCarrotName())
                .carrotAmt(entity.getCarrotAmt())
                .build()).collect(Collectors.toList());
    }

    @Transactional
    public CarrotModel addCarrot(CarrotModel carrotModel) {

        CarrotEntity carrotToLoad = CarrotEntity.builder().carrotAmt(carrotModel.getCarrotAmt())
                .carrotName(carrotModel.getCarrotName()).build();

        carrotRepository.save(carrotToLoad);
        carrotToLoad = carrotRepository.findCarrotModelByCarrotId(carrotToLoad.getCarrotId());

        return CarrotModel.builder().carrotId(carrotToLoad.getCarrotId())
                .carrotAmt(carrotToLoad.getCarrotAmt())
                .carrotName(carrotToLoad.getCarrotName())
                .build();

    }

    @Transactional
    public CarrotModel findCarrotByCarrotId(Long carrotId) {

        CarrotEntity foundCarrot = carrotRepository.findCarrotModelByCarrotId(carrotId);

        if(foundCarrot != null) {
           return CarrotModel.builder().carrotId(foundCarrot.getCarrotId())
                    .carrotAmt(foundCarrot.getCarrotAmt())
                    .carrotName(foundCarrot.getCarrotName())
                    .build();
        }
        else return null;
    }

    @Transactional
    public String deleteCarrotByCarrotyId(Long carrotId) {
        if(carrotRepository.findCarrotModelByCarrotId(carrotId) != null){
            carrotRepository.deleteByCarrotId(carrotId);
            return "Удалена морковь с id = " + carrotId;
        }
        return "Такой моркови нет, ничего не удалил";
    }
}
