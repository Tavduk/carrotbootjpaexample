package com.example.test.task.second.secondTry.Service;

import com.example.test.task.second.secondTry.Controller.CarrotController;
import com.example.test.task.second.secondTry.Model.CarrotModel;
import com.example.test.task.second.secondTry.SecondTryApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CarrotController.class)
public class CarrotServiceTests2 {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    CarrotService carrotService;

    CarrotModel carrot = CarrotModel.builder().carrotId(1L).carrotAmt(100L).carrotName("carrot1").build();
    CarrotModel carrot2 = CarrotModel.builder().carrotId(2L).carrotAmt(200L).carrotName("carrot2").build();
    CarrotModel carrot3 = CarrotModel.builder().carrotId(3L).carrotAmt(300L).carrotName("carrot3").build();

    @Test
    public void getAllCarrots() throws Exception {
        given(this.carrotService.findCarrotByCarrotId(1L)).willReturn(carrot);

        this.mockMvc.perform(get("http://localhost:8084/api/carrot/find?carrotId=1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
// .andExpect(content().string("Honda Civic"))
        ;
    }

}