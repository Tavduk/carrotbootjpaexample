package com.example.test.task.second.secondTry.Service;

import com.example.test.task.second.secondTry.Model.CarrotModel;
import com.example.test.task.second.secondTry.SecondTryApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SecondTryApplication.class)
public class CarrotServiceTests {

	@Autowired
	CarrotService carrotService;

	CarrotModel carrot = CarrotModel.builder().carrotId(1L).carrotAmt(100L).carrotName("carrot1").build();
	CarrotModel carrot2 = CarrotModel.builder().carrotId(2L).carrotAmt(200L).carrotName("carrot2").build();
	CarrotModel carrot3 = CarrotModel.builder().carrotId(3L).carrotAmt(300L).carrotName("carrot3").build();

	@Before
	public void setUp() {
		carrotService.addCarrot(carrot);
		carrotService.addCarrot(carrot2);
	}

	@Test
	public void testGetAllCarrots() {
		List<CarrotModel> carrotModels = carrotService.findAllCarrots();
		assertNotNull(carrotModels);
	}

	@Test
	public void testAddCarrot() {
		CarrotModel carrotModel = carrotService.addCarrot(carrot3);
		assertNotNull(carrotModel);
	}

	@Test
	public void testFindCarrot() {
		CarrotModel carrotModel = carrotService.findCarrotByCarrotId(1L);
		assertNotNull(carrotModel);
		assertEquals(carrot,carrotModel);
	}

	@Test
	public void testDeleteCarrot() {
		String result = carrotService.deleteCarrotByCarrotyId(1L);
		assertNotNull(result);
		assertEquals(result, new String("Удалена морковь с id = 1"));
	}

}
